package com.geopagos.prueba.figuras.factory;

import com.geopagos.prueba.figuras.data.entities.Circulo;
import com.geopagos.prueba.figuras.data.entities.Cuadrado;
import com.geopagos.prueba.figuras.data.entities.Geometrico;
import com.geopagos.prueba.figuras.data.entities.Triangulo;
import com.geopagos.prueba.figuras.exception.FiguraNotFoundException;
import org.springframework.stereotype.Component;

/**
 *
 * @author Leonardo
 */
@Component
public class FiguraFactory {

    public Geometrico getFigura(String tipoFigura) throws FiguraNotFoundException {

        switch (tipoFigura.toLowerCase()) {
            case "circulo":
                return new Circulo();
            case "triangulo":
                return new Triangulo();
            case "cuadrado":
                return new Cuadrado();
            default:
                throw new FiguraNotFoundException("La figura no es válida");
        }
    }

}
