package com.geopagos.prueba.figuras.data.repository;

import com.geopagos.prueba.figuras.data.entities.Cuadrado;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Leonardo
 */
public interface CuadradoRepository extends JpaRepository<Cuadrado, Long> {

    Cuadrado findOneById(long id);
}
