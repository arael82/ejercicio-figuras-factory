package com.geopagos.prueba.figuras.data.entities;

import java.io.Serializable;
import javax.persistence.Entity;

/**
 *
 * @author Leonardo
 */
@Entity(name = "triangulo")
@SuppressWarnings("PersistenceUnitPresent")
public class Triangulo extends AbstractFigura implements Geometrico, Serializable {
    
    @Override
    public Double calcularSuperficie() {
        return this.getBase() * (this.getAltura() / 2);
    }

    @Override
    public Double getDiametro() {
        return null;
    }

}
