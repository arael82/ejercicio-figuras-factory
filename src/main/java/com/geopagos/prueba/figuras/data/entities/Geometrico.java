package com.geopagos.prueba.figuras.data.entities;

/**
 *
 * @author Leonardo
 */
public interface Geometrico {

    Long getId(); 
    
    Double getBase();

    void setBase(Double base);

    Double getAltura();

    void setAltura(Double altura);

    Double calcularSuperficie();

    Double getDiametro();

    void setDiametro(Double diametro);

    void dibujar();

}
