package com.geopagos.prueba.figuras.data.entities;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author Leonardo
 */
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract public class AbstractFigura {

    private Long id;
    private Double base;
    private Double altura;
    private Double diametro;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    abstract public Double calcularSuperficie();

    @Column(name="base")
    public Double getBase() {
        return base;
    }

    public void setBase(Double base) {
        this.base = base;
    }

    @Column(name="altura")
    public Double getAltura() {
        return altura;
    }

    public void setAltura(Double altura) {
        this.altura = altura;
    }

    @Column(name="diametro")
    public Double getDiametro() {
        return diametro;
    }

    public void setDiametro(Double diametro) {
        this.diametro = diametro;
    }

    public void dibujar() {
        System.out.println("*****************************************");
        System.out.println("Atributos de: " + this.getClass().getName());
        System.out.println("Base: " + this.getBase());
        System.out.println("Altura: " + this.getAltura());
        System.out.println("Diámetro: " + this.getDiametro());
        System.out.println("Superficie: " + this.calcularSuperficie());
        System.out.println("*****************************************");
    }

}
