package com.geopagos.prueba.figuras.data.entities;

import java.io.Serializable;
import javax.persistence.Entity;

/**
 *
 * @author Leonardo
 */
@Entity(name="circulo")
@SuppressWarnings("PersistenceUnitPresent")
public class Circulo extends AbstractFigura implements Geometrico, Serializable {

    @Override
    public Double calcularSuperficie() {
        return Math.PI * Math.pow((double) this.getDiametro() / 2, 2);
    }

    @Override
    public Double getBase() {
        return null;
    }

    @Override
    public Double getAltura() {
        return null;
    }
    
}
