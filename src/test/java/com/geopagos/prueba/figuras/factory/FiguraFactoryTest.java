package com.geopagos.prueba.figuras.factory;

import com.geopagos.prueba.figuras.data.entities.Circulo;
import com.geopagos.prueba.figuras.data.entities.Geometrico;
import com.geopagos.prueba.figuras.exception.FiguraNotFoundException;
import org.junit.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Leonardo
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class FiguraFactoryTest {

    @Autowired
    FiguraFactory factory;

    @Test
    public void GivenFiguraFactory_WhenGetFiguraValidateSuccess_ThenOk()
            throws FiguraNotFoundException {
        Geometrico circulo = this.factory.getFigura("circulo");
        Assert.assertNotNull(circulo);
        Circulo circuloCast = (Circulo) circulo;
        Assert.assertNotNull(circuloCast);
    }

    @Test(expected = FiguraNotFoundException.class)
    public void GivenFiguraFactory_WhenGetFiguraValidateFiguraNotFound_ThenThrowException()
            throws FiguraNotFoundException {
        Geometrico circulo = this.factory.getFigura("pentagono");

    }

}
